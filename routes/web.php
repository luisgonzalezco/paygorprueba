<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/api/users', function (){

    return Datatables::eloquent(\App\Listado::query())->make(true);
    //dd((\App\Listado::query())->make(true));

});


//RUTAS DEL PANEL DE ADMIN

Route::group(['prefix' => 'admin', 'middleware' => 'auth', ], function(){


    Route::resource('listado','ListaController');

    Route::get('listado/{id}/destroy',[

        'uses' => 'ListaController@destroy',
        'as' => 'admin.listado.destroy'
    ]);


    Route::get('import',[

        'uses' => 'ListaController@getImport',
        'as' => 'admin.listado.import'

    ]);


    Route::post('import',[

        'uses' => 'ListaController@postImport',
        'as' => 'admin.listado.postimport'

    ]);



});