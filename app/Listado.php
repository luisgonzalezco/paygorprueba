<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listado extends Model
{
    //
    protected $table = "listados";
    protected $fillable = ['first_name', 'last_name'];

}
