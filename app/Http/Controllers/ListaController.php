<?php

namespace App\Http\Controllers;

use App\Listado;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Files\ExcelFile;
use Maatwebsite\Excel;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Facades\Datatables;
use Laracasts\Flash\Flash;

class ListaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        //$listados = Listado::all()->take(10);
        $listados = Listado::all();
        return view('admin.listado.index')->with('listados',$listados);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.listado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $listado = new Listado($request->all());
        $listado->save();

        //Flash::success("La modalidad " . $modalidad->nombre . " se ha registrado de manera exitosa!");
        return redirect()->route('listado.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $listado = Listado::find($id);
        return view('admin.listado.edit')->with('listado',$listado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $listado = Listado::find($id);
        $listado->fill($request->all());
        $listado->save();

        flash('User Edited ' . $listado->first_name . ' success', 'warning');
        return redirect()->route('listado.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $listado = Listado::find($id);
        $listado->delete();


        flash('User Deleted ' . $listado->first_name, 'danger');

        return redirect()->route('listado.index');
    }


    public function getImport(){

        return view('admin.listado.import.importUsuarios');


    }


    public function postImport(){
        \Excel::load(Input::file('import_file'),function($reader)
        {
            $reader-> each(function($sheet){
                Listado::create($sheet->toArray());


            });
        });

        echo "Se importaron de forma correcta los datos";
        flash('File imported ', 'success');
        return redirect()->route('listado.index');

    }
}
