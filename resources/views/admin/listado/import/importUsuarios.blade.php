@extends('layouts.app')

@section('title',' Import List')

@section('content')

  <div class="container">


    {!! Form::open(['route' => 'admin.listado.postimport', 'method' => 'POST', 'files' => 'true']) !!}

    <input type="file" name="import_file"/>

    <button class="btn btn-primary" action="{{ URL::to('postImport') }}">Import File</button>

    </form>

 </div>

@endsection