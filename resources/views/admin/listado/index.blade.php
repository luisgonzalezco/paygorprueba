@extends('layouts.app')

@section('title',' Lista de Usuarios')

@section('content')

    <div class="container">
    <div class="container-fluid">
    <div class="jumbotron">
        @include('flash::message')
        <a href="{{route('listado.create')}}" class="btn btn-info">Add User</a>


        <table class="table table-striped" id="listado">
            <thead>

            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Action</th>
            </thead>
            <tbody>
            @foreach($listados as $listado)
                <tr>

                    <td>{{ $listado->id }}</td>
                    <td>{{ $listado->first_name }}</td>
                    <td>{{ $listado->last_name }}</td>
                    <td> <a href="{{ route('listado.edit', $listado->id) }}" class="btn btn-warning">Edit</a>

                        <a href="{{ route('admin.listado.destroy', $listado->id) }}" onclick="return confirm('¿Está seguro que deseas eliminarlo?')" class="btn btn-danger">Delete</span></a>

                    </td>


                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    </div>
    </div>

@endsection

@section('js')


    <script type="text/javascript">

        $(document).ready(function(){

            $('#listado').DataTable();

        });

    </script>


@endsection