@extends('layouts.app')

@section('title',' Agregar usuario a la lista')

@section('content')

    <div class="container">

        {!! Form::open(['route' => 'listado.store', 'method' => 'POST']) !!}

        <div class="form-group">
            {!! Form::label('first_name', 'Name') !!}
            {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name...', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('last_name', 'Apellido') !!}
            {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name...', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
        </div>




        {!! Form::close() !!}

        <a href="{{ route('admin.listado.import') }}" class="btn btn-info">Import data</a>

    </div>



@endsection