@extends('layouts.app')

@section('title',' Editar usuario'. $listado->first_name)

@section('content')

    <div class="container">

        {!! Form::open(['route' => ['listado.update', $listado], 'method' => 'PUT']) !!}

        <div class="form-group">
            {!! Form::label('first_name', 'Name') !!}
            {!! Form::text('first_name', $listado->first_name, ['class' => 'form-control', 'placeholder' => 'First Name...', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('last_name', 'Last Name') !!}
            {!! Form::text('last_name', $listado->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name...', 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Save changes', ['class' => 'btn btn-success']) !!}
        </div>




        {!! Form::close() !!}



    </div>



@endsection